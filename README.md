# Spring Boot Sample SAML 2.0 Service Provider

#### Spring Boot

Spring Boot est un projet ou un micro framework qui a notamment pour but de faciliter la configuration d’un projet Spring et de réduire le temps alloué au démarrage d’un projet

- **Website:** [http://projects.spring.io/spring-boot/](http://projects.spring.io/spring-boot/)

#### Spring Security SAML Extension

Spring SAML Extension permet sans couture inclusion du SAML 2.0 Service Provider sous application basée sur Spring. Tous les produit qui supporte SAML 2.0 comme Fournisseur d’identité (ASFS 2.0, Shibboleth, OpenAM/OpenSSO, Ping Federate, Okta) peuvent être utiliser pour connecter avec Spring SAML Extension.

- **Website:** [http://projects.spring.io/spring-security-saml/](http://projects.spring.io/spring-security-saml/)

---------

## Description du Projet

Ce projet représente un exemple de mise en œuvre  du **SAML 2.0 Service Provider ** complètement construit avec **Spring Framework**. En particulier, celui-ci montre comment développer une solution web conçue pour l’Authentification Fédérée, en intégrant **Spring Boot** et **Spring Security SAML**. La configuration a été complètement défini en utilisant *Java annotations* (pas de xml).

---------

Le Service Provider est déployé comme application web. Enter [http://www.exemple.com:8081/](http://www.example.com:8081/) dans le fureteur pour voir l'application en cours d'exécution.

Vous pouvez télécharger les métadonnées sur [http://www.exemple.com:8081/saml/metadata](http://www.exemple.com:8081/saml/metadata).

---------

